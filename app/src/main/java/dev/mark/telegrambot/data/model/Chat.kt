package dev.mark.telegrambot.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import dev.mark.telegrambot.data.local.db.converter.Converter

@Entity(tableName = "chat")
@TypeConverters(Converter::class)
data class ChatOperation (
    @PrimaryKey
    var chatId: Long,
    var operation: OPERATION
)

enum class OPERATION(var text: String) {
    START("Привет, я покажу тебе информацию о Coronavirus\n\n\n" +
            "Операции: \n\t" +
            "По стране - отображение информации о заболевших по введенному названию страны\n\t" +
            "По местоположению - отображает данные о заболевших по стране вашего местонахождения\n\t" +
            "Добавить пациента - возможность добавить информацию о известных вам заболевшим.\n\t" +
            "Поиск в городе - поиск по городу добавленых заболевших.\n \n Выбери операцию:"),
    FIND_BY_NAME("Введите название страны латиницей:"),
    FIND_CITY_BY_NAME("Введите название города"),
    ADD_PATIENT_NAME("Введите имя пациента"),
    ADD_PATIENT_ADDRESS("Введите адресс пациента"),
    ADD_PATIENT_CITY("Введите город пациента"),
    ADD_PATIENT_SUCCESS("Добавлен"),
    ADD_PATIENT_ERROR("Не удалось добавить")
}