package dev.mark.telegrambot.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Patient(
    var name: String,
    var address: String,
    var city: String
){
    var cityNameLower = city
    @PrimaryKey(autoGenerate = true)
    var id = 0

    override fun toString(): String {
        return name + ": " + address
    }
}