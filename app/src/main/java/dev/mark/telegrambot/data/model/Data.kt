package dev.mark.telegrambot.data.model

data class Data(
    val active_cases: String,
    val country_name: String,
    val id: String,
    val new_cases: String,
    val new_deaths: String,
    val record_date: String,
    val region: Any,
    val serious_critical: String,
    val total_cases: String,
    val total_cases_per1m: String,
    val total_deaths: String,
    val total_recovered: String
) {
    override fun toString(): String {
        return "${country_name} \nВсего: ${total_cases} " +
                "\nНовые случаи: ${if (new_cases.isEmpty())"0" else new_cases}" +
                "\nВсего умерло:${total_deaths}" +
                "\nНовые случаи смертей: ${if (new_deaths.isEmpty())"0" else new_deaths} " +
                "\nВыздоровело: ${total_recovered} " +
                "\nВ критеческом состоянии: ${serious_critical}"
    }
}

data class ResponseData(
    val stat_by_country: List<Data>
)