package dev.mark.telegrambot.data

import android.annotation.SuppressLint
import dev.mark.telegrambot.data.local.db.AppDatabase
import dev.mark.telegrambot.data.model.*
import dev.mark.telegrambot.data.network.ApiRestService
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class Repository @Inject constructor(val database: AppDatabase, val apiRestService: ApiRestService){

    fun loadCountry(): Single<Country> {
        return apiRestService.loadCountry()
            .map { database.countryDao().clear()
                database.countryDao().insert(it.countries.map{it.toCountryName()})
                return@map it
            }
    }


    fun findCountry(countryName: String): Observable<ResponseData> {
        return Observable.fromCallable {
            database.countryDao().getCountry(countryName.toLowerCase())
        }.flatMap { countryName -> apiRestService.findDataByCountry(countryName.name) }
    }

    fun getByLocation(latitude: Float?, longitude: Float?): Observable<ResponseData> {
        return apiRestService.getNearestCities(latitude, longitude, 100000).
            flatMap { findCountry(it.first().country) }
    }
    fun getChatOperation(chatId: Long): Observable<ChatOperation> {
        return Observable.fromCallable { database.chatDao().getChat(chatId)}
    }

    fun insertChatOperation(chatId: Long, operation: OPERATION): Observable<Unit> {
         return Observable.fromCallable { database.chatDao().insert(ChatOperation(chatId, operation))}
    }

    fun addPatient(patient: Patient): Observable<Unit> {
        return Observable.fromCallable {
            database.patientDao().insert(patient)
        }
    }

    fun findDataByCity(city: String): Observable<List<Patient>> {
        return Observable.fromCallable {
            database.patientDao().getByCity(city.toLowerCase())
        }
    }

    fun getAllChatId(): Observable<List<Long>> {
        return Observable.fromCallable { database.chatDao().getChatIds()}
    }
}