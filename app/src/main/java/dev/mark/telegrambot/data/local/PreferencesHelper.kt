package mkm.com.coffee.data.local

import android.content.Context
import android.content.SharedPreferences
import dev.mark.telegrambot.Constants
import dev.mark.telegrambot.di.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesHelper @Inject
constructor(@ApplicationContext context: Context) {

    val pref: SharedPreferences


    init {
        pref = context.getSharedPreferences(
            "telegram_bot",
            Context.MODE_PRIVATE
        )
    }

    fun clear() {
        pref.edit().clear().apply()
    }
}
