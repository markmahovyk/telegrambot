package dev.mark.telegrambot.data.local.db


import androidx.room.Database
import androidx.room.RoomDatabase
import dev.mark.telegrambot.data.local.db.dao.CauntryDao
import dev.mark.telegrambot.data.local.db.dao.ChatOperationDao
import dev.mark.telegrambot.data.local.db.dao.PatientDao
import dev.mark.telegrambot.data.model.ChatOperation
import dev.mark.telegrambot.data.model.CountryName
import dev.mark.telegrambot.data.model.Patient

@Database(
    entities = [CountryName::class, ChatOperation::class, Patient::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun countryDao(): CauntryDao
    abstract fun chatDao(): ChatOperationDao
    abstract fun patientDao(): PatientDao
}
