package dev.mark.telegrambot.data.local.db.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dev.mark.telegrambot.data.model.CountryName

@Dao
abstract class CauntryDao {

    @Query("SELECT * FROM country where nameLower = :countryNameLower")
    abstract fun getCountry(countryNameLower: String): CountryName

    @Query("DELETE FROM country")
    abstract fun clear(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(userInfo: List<CountryName>)

}
