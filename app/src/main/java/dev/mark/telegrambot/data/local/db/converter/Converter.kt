package dev.mark.telegrambot.data.local.db.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dev.mark.telegrambot.data.model.OPERATION


class Converter {
    val gson = Gson()
//
    @TypeConverter
    fun stringToCart(data: String?): OPERATION {

        return OPERATION.valueOf(data!!)
    }

    @TypeConverter
    fun cartToString(list: OPERATION): String {
        return list.name
    }
}
