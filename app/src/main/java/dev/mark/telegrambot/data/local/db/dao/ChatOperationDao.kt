package dev.mark.telegrambot.data.local.db.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dev.mark.telegrambot.data.model.ChatOperation
import dev.mark.telegrambot.data.model.CountryName

@Dao
abstract class ChatOperationDao {
    @Query("SELECT * FROM chat where chatId = :countryNameLower")
    abstract fun getChat(countryNameLower: Long): ChatOperation

    @Query("SELECT chatId FROM chat")
    abstract fun getChatIds(): List<Long>

    @Query("DELETE FROM chat")
    abstract fun clear(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(userInfo: ChatOperation)

}
