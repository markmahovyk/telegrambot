package dev.mark.telegrambot.data.local.db.dao


import android.icu.text.CaseMap
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dev.mark.telegrambot.data.model.ChatOperation
import dev.mark.telegrambot.data.model.CountryName
import dev.mark.telegrambot.data.model.Patient

@Dao
abstract class PatientDao {
    @Query("SELECT * FROM patient where cityNameLower = :cityNameLower")
    abstract fun getByCity(cityNameLower: String): List<Patient>

    @Query("DELETE FROM patient")
    abstract fun clear(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(userInfo: Patient)
}
