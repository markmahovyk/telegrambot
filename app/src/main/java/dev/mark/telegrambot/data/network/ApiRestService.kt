package dev.mark.telegrambot.data.network

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.pengrad.telegrambot.model.OrderInfo
import dev.mark.telegrambot.BuildConfig
import dev.mark.telegrambot.data.model.Country
import dev.mark.telegrambot.data.model.CountryNearest
import dev.mark.telegrambot.data.model.Data
import dev.mark.telegrambot.data.model.ResponseData
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface ApiRestService {

    @GET("coronavirus/affected.php")
    fun loadCountry(): Single<Country>

  @GET("coronavirus/cases_by_particular_country.php")
    fun findDataByCountry(@Query("country") country: String): Observable<ResponseData>

    @GET("https://geocodeapi.p.rapidapi.com/GetNearestCities")
    fun getNearestCities(@Query("latitude") latitude: Float?, @Query("longitude") longitude: Float?, @Query("range") range: Int): Observable<List<CountryNearest>>


    companion object Factory {
        fun create(): ApiRestService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://coronavirus-monitor.p.rapidapi.com/")
                .client(provideOkHttpClient())
                .build()

            return retrofit.create(ApiRestService::class.java);
        }

        private fun provideOkHttpClient(): OkHttpClient {
            val okHttpClient = OkHttpClient.Builder()
            okHttpClient.addInterceptor { chain ->
                val newRequest = chain.request().newBuilder()
                    .addHeader("x-rapidapi-key", "89fd237458mshab8219757a6605bp16646fjsnebb9acf6a9b2")
                    .build()
                chain.proceed(newRequest)
            }

            okHttpClient.connectTimeout(10, TimeUnit.SECONDS)
            okHttpClient.readTimeout(10, TimeUnit.SECONDS)

            val logging = HttpLoggingInterceptor()
            logging.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            okHttpClient.addInterceptor(logging)

            return okHttpClient.build()
        }
    }
}