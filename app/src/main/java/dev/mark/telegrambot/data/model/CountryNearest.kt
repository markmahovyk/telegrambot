package dev.mark.telegrambot.data.model

import com.google.gson.annotations.SerializedName

data class CountryNearest(
    @SerializedName("City")
    val city: String,
    @SerializedName("Country")
    val country: String,
    val CountryId: String,
    val Distance: Double,
    val Latitude: Double,
    val Longitude: Double,
    val Population: Int
)