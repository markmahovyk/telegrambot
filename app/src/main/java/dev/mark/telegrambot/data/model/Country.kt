package dev.mark.telegrambot.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.PropertyKey

data class Country(
    @SerializedName("affected_countries")
    val countries: List<String>,
    val statistic_taken_at: String
)

@Entity(tableName = "country")
data class CountryName(
    var name: String,
    var nameLower: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}

fun String.toCountryName() = CountryName(
    this, this.toLowerCase()
)