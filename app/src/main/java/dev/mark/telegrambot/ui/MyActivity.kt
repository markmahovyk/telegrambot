package dev.mark.telegrambot.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup
import dev.mark.telegrambot.App
import dev.mark.telegrambot.R
import dev.mark.telegrambot.data.model.ChatOperation
import dev.mark.telegrambot.data.model.OPERATION
import dev.mark.telegrambot.data.model.Patient
import javax.inject.Inject


class MyActivity : AppCompatActivity(), MainMvpView {
    override fun editMessage(
        chatId: Long,
        messageId: Int?,
        text: String,
        markup: InlineKeyboardMarkup?
    ) {

        botHelper.editMessage(chatId,messageId, text, markup)
    }
var hashMap = hashMapOf<Long, Patient>()
    @Inject
    lateinit var presenter: MainPresenter
    lateinit var botHelper: BotHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as App).component?.inject(this)
        presenter.attachView(this)

        botHelper = BotHelper()
        botHelper.presenter = presenter

        setContentView(R.layout.activity_main)

        botHelper.bot.setUpdatesListener { updates ->
            updates.forEach {
                onUpdateReceived(it)
            }
            UpdatesListener.CONFIRMED_UPDATES_ALL
        }

        presenter.loadCountry()

    }

    override fun sendFirst(chatId: Long, product: String) {
        botHelper.sendMessageFirst(product, chatId)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    fun onUpdateReceived(update: Update) {

        val location = update.message().location()
        if (location != null) {
            presenter?.getDataByLocation(

                update.message().chat().id(),
                location.latitude(),
                location.longitude()
            )
            return
        }

        val message = update.message()

        //check if the update has a message
        val callbackquery = update.callbackQuery()
        if (callbackquery != null) {
            if (callbackquery.data() == "search_by_name") {
                presenter?.setOperation(message.chat().id(), OPERATION.FIND_BY_NAME, "Введите название страны для поиска")
            }

        } else if (message != null) {

            val input = message.text()

            if (input == "/start") {
                presenter.setOperation(update.message().chat().id(), OPERATION.START, OPERATION.START.text)
            }
            else if (input == BotHelper.Method.SEARCH_BY_COUNTRY.title) {
                presenter.setOperation(update.message().chat().id(), OPERATION.FIND_BY_NAME, OPERATION.FIND_BY_NAME.text)
            } else if (input == BotHelper.Method.SEARCH_BY_CITY.title) {
                presenter.setOperation(update.message().chat().id(), OPERATION.FIND_CITY_BY_NAME, OPERATION.FIND_CITY_BY_NAME.text)
            } else if (input == BotHelper.Method.ADD_PATIENT.title) {
                createDialogAddPatient(update)
            } else {
                presenter.getOperation(message.chat().id(), update)
            }
        }
    }

    override fun handleOperation(chatId: Long, update: Update, it: ChatOperation) {
        if (it.operation == OPERATION.FIND_BY_NAME) {
            presenter.findCountry(chatId, update.message().text())
        }else if (it.operation == OPERATION.FIND_CITY_BY_NAME) {
            presenter.findInCity(chatId, update.message().text())
        } else if (it.operation == OPERATION.ADD_PATIENT_NAME) {
            if (hashMap.containsKey(chatId)) {
                hashMap[chatId]?.name = update.message().text()
            } else {
                hashMap[chatId] = Patient(update.message().text(), "", "")
            }
            presenter.setOperation(update.message().chat().id(), OPERATION.ADD_PATIENT_ADDRESS, OPERATION.ADD_PATIENT_ADDRESS.text)
        } else if (it.operation == OPERATION.ADD_PATIENT_ADDRESS) {
            if (hashMap.containsKey(chatId)) {
                hashMap[chatId]?.address = update.message().text()
            }
            presenter.setOperation(update.message().chat().id(), OPERATION.ADD_PATIENT_CITY, OPERATION.ADD_PATIENT_CITY.text)
        }else if (it.operation == OPERATION.ADD_PATIENT_CITY) {
            if (hashMap.containsKey(chatId)) {
                hashMap[chatId]?.city = update.message().text()
                hashMap[chatId]?.cityNameLower = update.message().text().toLowerCase()
            }
            presenter.addPatient(chatId, hashMap[chatId])
        } else{
            sendFirst(update.message().chat().id(), "Выберите операцию")
        }
    }

    private fun createDialogAddPatient(update: Update) {
        presenter.setOperation(update.message().chat().id(), OPERATION.ADD_PATIENT_NAME, OPERATION.ADD_PATIENT_NAME.text)
    }
}