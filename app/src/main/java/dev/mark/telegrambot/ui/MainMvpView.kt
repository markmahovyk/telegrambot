package dev.mark.telegrambot.ui

import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup
import dev.mark.telegrambot.data.model.ChatOperation
import dev.mark.telegrambot.ui.base.MvpView

interface MainMvpView : MvpView {

    fun editMessage(chatId: Long, messageId: Int?, text: String, markup: InlineKeyboardMarkup?)
    fun sendFirst(chatId: Long, toString: String)
    fun handleOperation(chatId: Long, update: Update, it: ChatOperation)
}
