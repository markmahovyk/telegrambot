package dev.mark.telegrambot.ui


import android.annotation.SuppressLint
import com.pengrad.telegrambot.model.Update
import dev.mark.telegrambot.data.Repository
import dev.mark.telegrambot.data.model.OPERATION
import dev.mark.telegrambot.data.model.Patient
import dev.mark.telegrambot.ui.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainPresenter @Inject
internal constructor(
    val repository: Repository
) : BasePresenter<MainMvpView>() {
    private var disposable: CompositeDisposable? = CompositeDisposable()

    override fun detachView() {
        disposable?.dispose()
        super.detachView()
    }

    fun loadCountry() {
        disposable?.add(repository.loadCountry().subscribeOn(Schedulers.io()).subscribe({}, {it.printStackTrace()}))
    }

    fun findCountry(chatId: Long, countryName: String) {
        disposable?.add(repository.findCountry(countryName).subscribeOn(Schedulers.io()).subscribe({
                it ->
            val list = it.stat_by_country
            if (list.isNotEmpty()) {
                mvpView?.sendFirst(chatId, list[list.size - 1].toString())
            }
        }, {mvpView?.sendFirst(chatId, "Не удалось найти данных по указанной стране")}))
    }

    @SuppressLint("CheckResult")
    fun setOperation(
        chatId: Long,
        operation: OPERATION,
        text: String
    ) {
        repository.insertChatOperation(chatId, operation).subscribeOn(Schedulers.io()).subscribe({
            mvpView?.sendFirst(chatId, text)
        }, {
            mvpView?.sendFirst(chatId, "Бот не отвечает")
        })
    }

    @SuppressLint("CheckResult")
    fun getOperation(
        chatId: Long,
        update: Update
    ) {
        repository.getChatOperation(chatId).subscribeOn(Schedulers.io()).subscribe({
            mvpView?.handleOperation(chatId, update, it)
        }, {
            mvpView?.sendFirst(chatId, "Бот не отвечает")
        })
    }

    @SuppressLint("CheckResult")
    fun addPatient(chatId: Long,
                   patient: Patient?) {
        if (patient != null) {
            repository.addPatient(patient).subscribeOn(Schedulers.io()).subscribe({
                setOperation(chatId, OPERATION.ADD_PATIENT_SUCCESS, OPERATION.ADD_PATIENT_SUCCESS.text)
                sendAboutAddPatient(patient, chatId)
            }, {  setOperation(chatId, OPERATION.ADD_PATIENT_ERROR, OPERATION.ADD_PATIENT_ERROR.text)})

    }
}

    @SuppressLint("CheckResult")
    private fun sendAboutAddPatient(
        patient: Patient,
        chatId: Long
    ) {
        repository.getAllChatId().subscribeOn(Schedulers.io()).subscribe {
            val set = it.toMutableSet()
            set.remove(chatId)
            set.forEach {
                mvpView?.sendFirst(
                    it,
                    "В городе ${patient.city} обнаружен новый случай. Берегите себя и своих близких "

                ) }
        }
    }

    @SuppressLint("CheckResult")
    fun findInCity(chatId: Long, text: String?) {
        if (text != null) {
            repository.findDataByCity(text).subscribeOn(Schedulers.io()).subscribe(
                { mvpView?.sendFirst(chatId, "В этом городе: ${it.size} пациентов,\n " +
                        "${it.joinToString("\n")}")},
                {
                    mvpView?.sendFirst(
                        chatId,
                        "В вашем городе пока нет инфецырованых или они хорошо скрываються"
                    )}

            )
        }
    }

    fun getDataByLocation(chatId: Long, latitude: Float?, longitude: Float?) {
        disposable?.add(
            repository.getByLocation(
                latitude,
                longitude
            ).subscribeOn(Schedulers.io()).subscribe({ it ->
                val list = it.stat_by_country
                if (list.isNotEmpty()) {
                    mvpView?.sendFirst(
                        chatId,
                        "Страна вашего место расположения:" + list[list.size - 1].toString()
                    )
                }
            }, { mvpView?.sendFirst(chatId, "Не удалось разпознать страну вашего местоположения") })
        )
    }
}



