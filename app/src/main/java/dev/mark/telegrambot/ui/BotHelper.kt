package dev.mark.telegrambot.ui

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.Update
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup
import com.pengrad.telegrambot.model.request.KeyboardButton
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup
import com.pengrad.telegrambot.request.EditMessageText
import com.pengrad.telegrambot.request.SendMessage
import dev.mark.telegrambot.data.model.OPERATION


class BotHelper() {

    val API_TOKEN = "1060223096:AAEVn8yk4mJUGTR9jY_jtvVCLNEESaLlo8U"
    val bot = TelegramBot(API_TOKEN)

    enum class Method(var title: String){
        SEARCH_BY_COUNTRY("По стране"),
        ADD_PATIENT("Добавить пациента"),
        SEARCH_BY_LOCATION("По местоположению"),
        SEARCH_BY_CITY("Поиск в городе")
    }


    var presenter: MainPresenter ? = null

    fun sentMessage(chatId: Long, text: String) {
        val sendMessage = createMessage(chatId, text)

        bot.execute(sendMessage)
    }

    private fun createMessage(
        chatId: Long,
        text: String
    ): SendMessage {
        val sendMessage = SendMessage(chatId, text)
        // Создаем список строк клавиатуры
        val keyboard = mutableListOf<KeyboardButton>()

        // Первая строчка клавиатуры
        val keyboardFirstRow = KeyboardButton(Method.SEARCH_BY_COUNTRY.title)

        keyboard.add(keyboardFirstRow)
        keyboard.add(addButtonRequestLocation())
        keyboard.add(KeyboardButton(Method.ADD_PATIENT.title))
        keyboard.add(KeyboardButton(Method.SEARCH_BY_CITY.title))


        val replyKeyboardMarkup = ReplyKeyboardMarkup(keyboard.toTypedArray())
        sendMessage.replyMarkup(replyKeyboardMarkup)
        replyKeyboardMarkup.selective(true)
        replyKeyboardMarkup.resizeKeyboard(true)
        replyKeyboardMarkup.oneTimeKeyboard(false)
        return sendMessage
    }

    private fun addButtonRequestLocation(): KeyboardButton {
        val keyboardSecondRow = KeyboardButton(Method.SEARCH_BY_LOCATION.title)
        keyboardSecondRow.requestLocation(true)
        return keyboardSecondRow
    }

    fun editMessage(chatId: Long, messageId: Int?, text: String, markup: InlineKeyboardMarkup?) {
        val editMarkup = messageId?.let {
            EditMessageText(
                chatId,
                it,
                text
            )
        }
        editMarkup?.replyMarkup(markup)

        bot.execute(editMarkup)
    }

    fun sendMessageFirst(text: String, chatId: Long) {
        bot.execute(createMessage(chatId, text))
    }
}
