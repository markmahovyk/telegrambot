package dev.mark.telegrambot.ui.base


interface Presenter<V : MvpView> {
    fun attachView(mvpView: V)
    fun detachView()

}
