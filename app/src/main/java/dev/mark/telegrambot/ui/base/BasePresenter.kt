package dev.mark.telegrambot.ui.base

open class BasePresenter<T : MvpView> : Presenter<T> {

    var mvpView: T? = null
        private set

    private val isViewAttached: Boolean
        get() = mvpView != null

    override fun attachView(mvpView: T) {
        this.mvpView = mvpView
    }

    override fun detachView() {
        mvpView = null
    }

    fun checkViewAttached() {
        if (!isViewAttached) throw MvpViewNotAttachedException()
    }

    class MvpViewNotAttachedException :
        RuntimeException("Please call MainPresenter.attachView(MvpView) before requesting data to the MainPresenter") {
    }

}
