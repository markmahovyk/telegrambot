package dev.mark.telegrambot.di


import android.app.Application
import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dev.mark.telegrambot.data.local.db.AppDatabase
import dev.mark.telegrambot.data.network.ApiRestService
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @ApplicationContext
    internal fun provideContext(): Context {
        return application
    }

    @Provides
    @Singleton
    internal fun providesApplication(): Application {
        return application
    }

    @Provides
    @Singleton
    internal fun provideApiRestService(): ApiRestService {
        return ApiRestService.create()
    }

    @Provides
    @Singleton
    internal fun provideAppDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(
            application,
            AppDatabase::class.java,
            "telegram_bot"
        )
            .fallbackToDestructiveMigration()
            .build()
    }
}