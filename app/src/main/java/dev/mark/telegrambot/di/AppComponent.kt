package dev.mark.telegrambot.di

import dagger.Component
import dev.mark.telegrambot.ui.BotHelper
import dev.mark.telegrambot.ui.MyActivity
import mkm.com.coffee.data.local.PreferencesHelper
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(activity: BotHelper)


    fun preferencesHelper(): PreferencesHelper

    fun inject(activity: MyActivity)
}