package dev.mark.telegrambot


import android.app.Application
import dev.mark.telegrambot.di.AppComponent
import dev.mark.telegrambot.di.AppModule
import dev.mark.telegrambot.di.DaggerAppComponent
import io.reactivex.plugins.RxJavaPlugins


class App : Application() {

    val component: AppComponent?
        get() = appComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

        RxJavaPlugins.setErrorHandler { }
    }

    companion object {
        private var appComponent: AppComponent? = null
    }
}
