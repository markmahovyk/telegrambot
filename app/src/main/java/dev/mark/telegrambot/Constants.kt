package dev.mark.telegrambot


object Constants {

    const val PREF_FSM_TOKEN = "pref_fsm_token"
    const val PREF_USER_ID = "pref_user_id"
    const val PREF_NEED_CHECK_VERSION = "pref_need_check_version"

    const val PREF_CONTACT_PHONE = "pref_contact_phone"

    const val TIME_DELAY = 500L
    const val NOTIFICATION_LIMIT = 20
    const val NOTIFICATION_ID = 10

    const val DAY_PARAM = "dd MMMM"
    const val HOUR_PARAM = "HH"
    const val MINUTE_PARAM = "mm"
    const val FULL_DATA_PARAM = "dd MMMM HH:mm"

    const val CURRENCY = "UAH"
}
